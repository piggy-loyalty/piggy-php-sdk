<?php

namespace Piggy\Api\Exceptions;

use Exception;

class BadResponseException extends Exception
{
}