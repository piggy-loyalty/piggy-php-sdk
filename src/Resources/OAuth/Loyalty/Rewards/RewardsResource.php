<?php

namespace Piggy\Api\Resources\OAuth\Loyalty\Rewards;

use Piggy\Api\Exceptions\RequestException;
use Piggy\Api\Mappers\Loyalty\Rewards\RewardsMapper;
use Piggy\Api\Models\Shops\Shop;
use Piggy\Api\Resources\BaseResource;

/**
 * Class RewardsResource
 * @package Piggy\Api\Resources\OAuth\Loyalty\Rewards
 */
class RewardsResource extends BaseResource
{
    /**
     * @var string
     */
    protected $resourceUri = "/api/v2/oauth/clients/rewards";

    /**
     * @param Shop $shop
     * @return array
     * @throws RequestException
     */
    public function all(Shop $shop): array
    {
        $response = $this->client->get($this->resourceUri, [
            "shop_id" => $shop->getId(),
        ]);

        $mapper = new RewardsMapper();

        return $mapper->map($response->getData());
    }
}
